<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> महर्षि दयानन्द इण्टर कॉलेज
    </title>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

    <!-- bootstrap4 css link -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- bootstrap4 js and jquery links -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

</head>

<body class="contactbody">
    <?php include('header.php') ?>>

            <div class="mt-5">
    <section>
        <div class="container">
            <div class="section-title text-center mb-4">
                <h3>संपर्क करें</h3>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="container-fluid">
                        <div class="map-responsive">
                            <div class="">
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3470.408073236693!2d78.27396521446032!3d29.562727848123444!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390be138cc42fbfd%3A0x2a9d0f07126385df!2sMaharshi%20Dayanand%20Inter%20College!5e0!3m2!1sen!2sin!4v1633673484981!5m2!1sen!2sin"
                                    width="600" height="450" style="border:0;" allowfullscreen=""
                                    loading="lazy"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="contactform">
                    <form class="p-4 text-center">
                        <div class="row">
                            <div class="col mb-2">
                                <label for="exampleInputname">आपका नाम</label>
                                <input type="text" class="form-control" id="exampleInputname"
                                    aria-describedby="emailHelp" placeholder="आपका नाम">
                            </div>
                            <div class="col mb-2">
                                <label for="exampleInputEmail1">ईमेल</label>
                                <input type="email" class="form-control" id="exampleInputEmail1"
                                    aria-describedby="emailHelp" placeholder="ईमेल">
                            </div>
                        </div>
                        <div class="form-group mb-2">
                            <label for="exampleInputsubject">विषय</label>
                            <input type="text" class="form-control" id="exampleInputsubject" placeholder="विषय">
                        </div>
                        <div class="form-group mb-2">
                            <label for="exampleFormControlTextarea1">संदेश</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                        </div>
                        <button type="submit" class="btn submitbtn mt-3">प्रस्तुत</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    </div>


    <?php include('footer.php')?>


</body>

</html>