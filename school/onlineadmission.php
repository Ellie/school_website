<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> महर्षि दयानन्द इण्टर कॉलेज
    </title>
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

    <!-- bootstrap4 css link -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <!-- bootstrap4 js and jquery links -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>

</head>

<body>
    <?php include('header.php') ?>
    <div class="onlineadmission">
    <div class="container">
        <div class="row mt-3">
        <div class="col-md-12 text-center">
                <h3>Online Admission</h3>
        </div>
</div>
<div class="row">
            <div class="col-md-12 text-right">
                <!-- Button trigger modal -->
                <button type="button" class="btn submitbtn" data-toggle="modal" data-target="#exampleModal">
                    Check your form status
                </button>
</div>
</div>
                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                    aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"> Check your form status
                                </h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col">
                                        <label for="inputState"> Enter Your Reference Number </label>

                                        <input type="number" class="form-control" placeholder="">
                                    </div>
                                    <div class="col">
                                        <label for="inputState">Date of Birth</label>

                                        <input type="date" class="form-control" placeholder="">
                                    </div>

                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">submit</button>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <div class="container">
        <div class="card mt-4">
            <div class="card-header py-1">
                <h4>Instructions</h4>
            </div>
            <div class="card-body">
                <h5 class="card-title">Please enter your institution online admission instructions here.</h5>
            </div>
        </div>
        <div class="card mt-4">
            <div class="card-header py-1">
                <h4>Basic Detail</h4>
            </div>
            <div class="card-body">
                <form>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="inputState">class</label>
                            <select id="inputState" class="form-control">
                                <option>Select</option>
                                <option>class1</option>
                                <option>class2</option>
                                <option>class3</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="inputState">First name</label>

                            <input type="text" class="form-control" placeholder="">
                        </div>
                        <div class="col-md-4">
                            <label for="inputState">Last name</label>

                            <input type="text" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="inputState">Gender</label>
                            <select id="inputState" class="form-control">
                                <option>Select</option>
                                <option>Male</option>
                                <option>Female</option>
                            </select>
                        </div>

                        <div class="col-md-4">
                            <label for="inputState">Date of Birth</label>
                            <input type="date" class="form-control" placeholder="">
                        </div>
                        <div class="col-md-4">
                            <label for="inputState">Mobile Number</label>

                            <input type="number" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="inputState">Email</label>
                            <input type="email" class="form-control" placeholder="">
                        </div>
                        <div class="col-md-4">
                            <label for="inputState">Student photo</label>
                            <input type="file" class="form-control" placeholder="">
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="card mt-4">
            <div class="card-header py-1">
                <h4>Guardian Details</h4>
            </div>
            <div class="card-body">
                <form>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="inputState"> If Guardian Is
                            </label>
                            <select id="inputState" class="form-control">
                                <option>Select</option>
                                <option>Mother</option>
                                <option>Father</option>
                                <option>Other</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="inputState">
                                Guardian Name
                            </label>

                            <input type="text" class="form-control" placeholder="">
                        </div>
                        <div class="col-md-4">
                            <label for="inputState">
                                Guardian Relation
                            </label>

                            <input type="text" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="row">
                        <div class=" col-md-4">
                            <label for="inputState"> Guardian Email</label>
                            <input type="email" class="form-control" placeholder="">
                        </div>

                        <div class="col-md-4">
                            <label for="inputState"> Guardian Photo </label>
                            <input type="file" class="form-control" placeholder="">
                        </div>
                        <div class="col-md-4">
                            <label for="inputState">
                                Guardian Phone
                            </label>
                            <input type="number" class="form-control" placeholder="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label for="inputState">
                                Guardian Occupation
                            </label>
                            <input type="text" class="form-control" placeholder="">
                        </div>
                        <div class="col-md-4">
                            <label for="inputState">
                                Guardian Address
                            </label>
                            <textarea name="" class="form-control"></textarea>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        <div class="card mt-4">
            <div class="card-header py-1">
                <h4>
                    Miscellaneous Details
                </h4>
            </div>
            <div class="card-body">
                <form>
                    <div class="col-md-4">
                        <label for="inputState">
                            National Identification Number
                        </label>

                        <input type="text" class="form-control" placeholder="">
                    </div>
                    <div class="col-md-4">
                        <label for="inputState">
                            Previous School Details
                        </label>
                        <textarea name="" class="form-control"></textarea>
                    </div>
                </form>
            </div>
        </div>
        <div class="card mt-4">
            <div class="card-header py-1">
                <h4>
                    Upload Documents
                </h4>
            </div>
            <div class="card-body">
                <form>
                    <div class="col-md-4">

                        <label for="inputState"> Documents (To upload multiple document compress it in a single file
                            then upload it) </label>
                        <input type="file" class="form-control" placeholder="">
                    </div>
                </form>
            </div>
        </div>
        <div class="row my-4">
           <div class="col-md-12 text-center">
           <button type="submit" class="btn submitbtn mt-3">Submit</button>
           </div>
        </div>
    </div>

    </div>
    <?php include('footer.php')?>


</body>

</html>